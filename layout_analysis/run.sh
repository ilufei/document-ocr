#!/usr/bin/env bash

DATA_NAME=book
IMAGE_DIR=/home/zhengwu/data/book_layout
TRAIN_LABEL_LIST=/home/zhengwu/data/book_layout/train.list
BATCH_SIZE=4
NUM_CLASS=2

python train.py --train_label_list ${TRAIN_LABEL_LIST} --image_dir ${IMAGE_DIR} \
 --batch_size ${BATCH_SIZE} --num_class ${NUM_CLASS} --data_name ${DATA_NAME}
